Title: Mon Super Titre
Date: 2020-08-15 18:58
Modified: 2020-08-15 18:58
Category: Blog
Tags: pelican, markdown, gitlab, github, ci/cd
Slug: mon-permier-post
Authors: Ali Akrour
Summary: Un blog généré automatiquement par CI/CD

L'objectif est de mettre en place un pipeline d'intégration et de déploiement continu, qui va recevoir sur un dépot GitLab personnel des articles de blog écrit en Markdown, les convertir automatiquement en HTML, et les publier sur GitHub Pages.
Dans le pipeline, on pourra ajouter des étapes de test et de qualité du markdown et du html.
